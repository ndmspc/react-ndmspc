import React from "react";
import '/node_modules/@ndmspc/react-ndmspc-patternfly/dist/style.css';
import { NdmspcApp , NdmspcBinPlugin } from "@ndmspc/react-ndmspc-patternfly";
// import { RsnBinPlugin, RsnProjectionPlugin } from "@ndmspc/react-ndmspc-plugins";

const plugins = {
  // RsnBinPlugin,
  // RsnProjectionPlugin,
  NdmspcBinPlugin
}

export default function App() {
  return (<NdmspcApp plugins={plugins} />);
}
