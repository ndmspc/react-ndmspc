# @ndmspc/react-ndmspc

[![NPM](https://img.shields.io/npm/v/@ndmspc/react-ndmspc.svg)](https://www.npmjs.com/package/@ndmspc/react-ndmspc) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @ndmspc/react-ndmspc
```

## Usage

```jsx
import React from "react";
import '/node_modules/@ndmspc/react-ndmspc-patternfly/dist/style.css';
import { NdmspcApp } from "@ndmspc/react-ndmspc-patternfly";
import { RsnBinPlugin, RsnProjectionPlugin } from "@ndmspc/react-ndmspc-plugins";

export default function App() {
  return (<NdmspcApp plugins={{ RsnBinPlugin, RsnProjectionPlugin }} />);
}

```

## License

MIT © [ndmspc](https://gitlab.com/ndmspc/react-ndmspc)
