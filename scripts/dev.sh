#!/bin/bash
NDMSPC_CMD=${1-dev}
NDMSPC_CLEAN=${NDMSPC_CLEAN=0}
NDMSPC_BASE_DIR=${NDMSPC_BASE_DIR-"$HOME/git/gitlab.com/ndmspc"}
NDMSPC_DEP_PROJECTS=${NDMSPC_DEP_PROJECTS-"react-ndmspc-core ndmvr react-ndmspc-patternfly react-ndmspc-plugins"}
NDMSPC_RELEASE_VERSION=${NDMSPC_RELEASE_VERSION-"~0.20231212.1"}

# Hint: Use 'https://www.educative.io/answers/how-to-configure-git-to-ignore-some-files-locally' to ignore package.json to be commited while using npm version for deps as file:

# git update-index --assume-unchanged path/to/file.txt
# git update-index --no-assume-unchanged path/to/file.txt
# git ls-files -v | grep '^[[:lower:]]'

CDPATH=
if ! command -v screen &> /dev/null; then
  echo "Warning: 'screen' is not installed !!! Please install it first ('dnf install screen' or similar)  ..."
  exit 1
fi

[ -d "$NDMSPC_BASE_DIR" ] || { echo "Direcrtory defined in '\$NDMSPC_BASE_DIR' doesn't exists !!! Do `$NDMSPC_BASE_DIR=<path to ndmspc projects>`"; exit 1 ;}

for d in $NDMSPC_DEP_PROJECTS;do
  [ -d "$NDMSPC_BASE_DIR/$d" ] || { echo "Project direcrtory '$NDMSPC_BASE_DIR/$d' doesn't exists !!! Modify NDMSPC_DEP_PROJECTS variable !!! Current value NDMSPC_DEP_PROJECTS='"$NDMSPC_DEP_PROJECTS"'"; exit 1 ;}
done


for d in $NDMSPC_DEP_PROJECTS;do
    # git update-index --assume-unchanged $NDMSPC_BASE_DIR/$d/package.json
    # git update-index --no-assume-unchanged $NDMSPC_BASE_DIR/$d/package.json
  tmp=$(mktemp)
  # echo "$NDMSPC_BASE_DIR/$d/package.json $tmp"
  for d2 in $NDMSPC_DEP_PROJECTS;do   
    [[ $d == $d2 ]] && continue
    # echo jq -r '.dependencies."@ndmspc/'$d2'"' $NDMSPC_BASE_DIR/$d/package.json
    if [[ $NDMSPC_CMD == release ]];then
      if [[ $(jq -r '.dependencies."@ndmspc/'$d2'"' $NDMSPC_BASE_DIR/$d/package.json) != null ]];then
        echo "$NDMSPC_BASE_DIR/$d/package.json $d2"
        jq '.dependencies."@ndmspc/'$d2'"="'$NDMSPC_RELEASE_VERSION'"' $NDMSPC_BASE_DIR/$d/package.json > "$tmp" 
        mv "$tmp" $NDMSPC_BASE_DIR/$d/package.json
      fi
    else
      if [[ $(jq -r '.dependencies."@ndmspc/'$d2'"' $NDMSPC_BASE_DIR/$d/package.json) != null ]];then
        echo "$NDMSPC_BASE_DIR/$d/package.json $d2"
        jq '.dependencies."@ndmspc/'$d2'"="file:../'$d2'"' $NDMSPC_BASE_DIR/$d/package.json > "$tmp" 
        mv "$tmp" $NDMSPC_BASE_DIR/$d/package.json
      fi
    fi
  done
done

if [[ $NDMSPC_CMD == dev ]] || [[ $NDMSPC_CMD == kill ]] ;then
  for d in $NDMSPC_DEP_PROJECTS;do
    if screen -list | grep -q $d; then
      echo "Killing currently running screen session '$d' ..."
      screen -XS $d quit 2>1 >/dev/null
    fi
    echo "Doing npm install in '$NDMSPC_BASE_DIR/$d' "
    

    if [[ $NDMSPC_CMD != kill ]];then
      cd $NDMSPC_BASE_DIR/$d
      [[ $NDMSPC_CLEAN == 1 ]] && rm -rf $NDMSPC_BASE_DIR/$d/node_modules
      rm -f package-lock.json
      npm install || { echo "Problem with 'npm install' in '$NDMSPC_BASE_DIR/$d' !!!"; exit 1; }
      npm run build || { echo "Problem with 'npm build' in '$NDMSPC_BASE_DIR/$d' !!!"; exit 1; }
      cd -
      screen -S $d -dm bash -c "cd $NDMSPC_BASE_DIR/$d; npm run watch; exec bash;"
      echo "Access project '$d' by running 'screen -x $d' ...";
    fi
  done

  echo "Run:"
  echo "     npm install"
  echo "     npm run dev"
fi

echo "List of screens:"
screen -ls
