FROM node:20 AS builder
WORKDIR /builder
COPY package*.json /builder/
COPY vite*.js /builder/
COPY index.html /builder/
COPY src /builder/src
RUN npm i
RUN npm run build-app

FROM docker.io/nginxinc/nginx-unprivileged:alpine
WORKDIR /usr/share/nginx/html
COPY --from=builder /builder/dist/ /usr/share/nginx/html
